<?php include 'header.php'; ?>
<div class="blog categoriaBlog">
	<section class="seccion1">
		<div class="contenedor">
			<div class="conteLeft">

				<?php

				//OBTENER EL TOTAL DE ARTICULOS
				$totalArticulos = count($newData->articles);

				define('NUM_ITEMS_BY_PAGE', 5);
				if($totalArticulos > 0){
					$page = false;

				    //examino la pagina a mostrar y el inicio del registro a mostrar
				    if(isset($_GET["page"])) {
				        $page = $_GET["page"];
				    }
				    $numMostrar = 0;

				    $paginaInicio = 0;
				    $paginaFin = NUM_ITEMS_BY_PAGE;

				    if(!$page){
						$start = 0;
						$page = 1;
					}else{
						$start = ($page - 1) * NUM_ITEMS_BY_PAGE;
						$numMostrar = ($page * $paginaFin)-5;
					}

				    $total_pages = ceil($totalArticulos / NUM_ITEMS_BY_PAGE);		    

				    if($total_pages > 0) {

						/*?>
						<pre> 
						<?php
							//print_r(array_slice($newData->articles, 4, 6));
							//print_r ($newData->articles); 
						?> 
						</pre>
						<?php
						*/

						$salida = array_slice($newData->articles, $numMostrar, $paginaFin);
						//$salida = $newData->articles;

						foreach($salida as $News){
							$productsTitles = $News->title;
							$images = $News->urlToImage;
							$descripcion = $News->description;
							$autor = $News->author;
							$fechaP = $News->publishedAt;
							$urlT = $News->url;
							$urlNav = $News->url;

							//FECHA PUBLICACION
							$dia = date('d', strtotime($News->publishedAt));
							$mes = date('F', strtotime($News->publishedAt));
							$anio = date('Y', strtotime($News->publishedAt));
							if($mes == 'January') { $mes = 'Enero'; }
							if($mes == 'February') { $mes = 'Febrero'; }
							if($mes == 'March') { $mes = 'Marzo'; }
							if($mes == 'April') { $mes = 'Abril'; }
							if($mes == 'May') { $mes = 'Mayo'; }
							if($mes == 'June') { $mes = 'Junio'; }
							if($mes == 'July') { $mes = 'Julio'; }
							if($mes == 'August') { $mes = 'Agosto'; }
							if($mes == 'September') { $mes = 'Septiembre'; }
							if($mes == 'October') { $mes = 'Octubre'; }
							if($mes == 'November') { $mes = 'Noviembre'; }
							if($mes == 'December') { $mes = 'Diciembre'; }

							$nFecha = $dia.' de '.$mes.', '.$anio;

							?>
							<div class="itemPost">
				            	<div class="bg" style="background-image: url(<?=$images?>);">
				            		<ul class="col2 infoPost">
										<li>
											<a href="blog-categoria/s"><i class="fas fa-folder"></i>s</a>
										</li>
										<li>
											<i class="fas fa-calendar-alt"></i><?=$nFecha?>
										</li>
										<div class="clear"></div>
									</ul>
				            	</div>
				            	<h4 class="tituloP"><?=$productsTitles?></h4>
				            	<p class="descP"><?=$descripcion?></p>
				            	<!-- <a href="<?=$url?>" class="verMas">ver más</a> -->
				            	<a href="<?=_ROOT?>post/<?=tituloUrl($productsTitles)?>" class="verMas">ver más</a>
				            </div>
							<?php
						}				    	
				    }

				    echo '<div class="clear"></div>';
				    echo '<nav>';
				    echo '<ul class="pagination">';

				    if($total_pages > 1) {
				        if ($page != 1) {
				            echo '<li class="page-item"><a class="page-link" href="'._ROOT.'blog/'.($page-1).'"><span aria-hidden="true">&laquo;</span></a></li>';
				        }

				        for($i=1;$i<=$total_pages;$i++){
				            if ($page == $i) {
				                echo '<li class="page-item active"><a class="page-link" href="#">'.$page.'</a></li>';
				            }else{
				                echo '<li class="page-item"><a class="page-link" href="'._ROOT.'blog/'.$i.'">'.$i.'</a></li>';
				            }
				        }

				        if($page != $total_pages){
				            echo '<li class="page-item"><a class="page-link" href="'._ROOT.'blog/'.($page+1).'"><span aria-hidden="true">&raquo;</span></a></li>';
				        }
				    }
				    echo '</ul>';
				    echo '</nav>';
				}

				?>
			</div>

			<div class="conteRight"><?php include 'blog-derecho.php'; ?></div>
			<div class="clear"></div>
		</div>
	</section>
</div>
<?php include 'footer.php'; ?>