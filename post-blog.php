<?php 
include 'header.php';

if(isset($url_noticia)){
	$postCorrecto = false;
	foreach($newData->articles as $News){
		
		
		if($url_noticia == tituloUrl($News->title)){
			$images = $News->urlToImage;
			$titulo = $News->title;
			$descripcion = $News->description;
			$autor = $News->author;
			$fechaP = $News->publishedAt;
			$url = $News->url;
			$urlNav = $News->url;

			//FECHA PUBLICACION
			$dia = date('d', strtotime($fechaP));
			$mes = date('F', strtotime($fechaP));
			$anio = date('Y', strtotime($fechaP));
			if($mes == 'January') { $mes = 'Enero'; }
			if($mes == 'February') { $mes = 'Febrero'; }
			if($mes == 'March') { $mes = 'Marzo'; }
			if($mes == 'April') { $mes = 'Abril'; }
			if($mes == 'May') { $mes = 'Mayo'; }
			if($mes == 'June') { $mes = 'Junio'; }
			if($mes == 'July') { $mes = 'Julio'; }
			if($mes == 'August') { $mes = 'Agosto'; }
			if($mes == 'September') { $mes = 'Septiembre'; }
			if($mes == 'October') { $mes = 'Octubre'; }
			if($mes == 'November') { $mes = 'Noviembre'; }
			if($mes == 'December') { $mes = 'Diciembre'; }

			$nFecha = $dia.' de '.$mes.', '.$anio;


			$postCorrecto = true;
		}
	}
}

if($postCorrecto){
	?>
	<div class="blog">
		<section class="seccion1">
			<div class="contenedor">
				<div class="conteLeft">
					<div class="imgPost">
						<img src="<?=$images?>" alt="<?=$titulo?>">
						<ul class="col2 infoPost">
							<li>
								<a href="javascript:void(0)"><i class="fas fa-folder"></i>Categoria</a>
							</li>
							<li>
								<i class="fas fa-calendar-alt"></i><?=$nFecha?>
							</li>
							<div class="clear"></div>
						</ul>
					</div>
					<h1 class="tituloPost"><?=$titulo?></h1>
					<h1 class="autorPost">Autor: <?=$autor?></h1>
					<div class="descPost"><?=$descripcion?></div>
					<div class="clear20"></div>
					<div class="conteCompartir">
						<h3>compartir</h3>
						<ul class="col4">
							<li>
								<a href="mailto:?body=¡Hola!%0A%0Ahe encontrado este nuevo artículo y pensé que podría interesarte.%0A%0A¡Saludos!%0A%0A<?= _ROOT.'post/'.$url?>&subject=han compartido un artículo contigo" class="conte">
									<i class="fa fa-envelope" aria-hidden="true"></i> <span>EMAIL</span>
								</a>
							</li>
							<li>
								<a href="javascript:void(0)" class="conte" onclick="window.open('http://www.facebook.com/sharer/sharer.php?u=<?=_ROOT.'post/'.$url?>','share','width=500,height=500')">
									<i class="fab fa-facebook-f" aria-hidden="true"></i>
								</a>
							</li>
							<li>
								<a href="javascript:void(0)" class="conte" onclick="window.open('https://twitter.com/intent/tweet?text=<?='Leyendo: '._ROOT.'post/'.$url?>', 'share', 'width=500,height=300')">
									<i class="fab fa-twitter" aria-hidden="true"></i>
								</a>
							</li>
							<li>
								<a href="https://api.whatsapp.com/send?text=<?=_ROOT.'post/'.$url?>" data-action="share/whatsapp/share" target="_blank" class="conte"><i class="fab fa-whatsapp" aria-hidden="true"></i></a>
							</li>
							<div class="clear"></div>
						</ul>
					</div>
				</div>
				<div class="conteRight"><?php include 'blog-derecho.php'; ?></div>
				<div class="clear"></div>
			</div>
		</section>
	</div>
	<?php
}


exit;

if(isset($url_noticia)&&isset($url_perfil)){
	if(mysqli_num_rows($link->query("SELECT * FROM categoria_perfil WHERE link = '$url_perfil' LIMIT 1"))>0 && mysqli_num_rows($link->query("SELECT * FROM blog WHERE link = '$url_noticia' LIMIT 1"))>0){
		$rowC = $link->query("SELECT * FROM categoria_perfil WHERE link = '$url_perfil' LIMIT 1")->fetch_array();
		$imgExtra1 = _ROOT.$rowC['ruta_img_extra1'];
		$imgExtra2 = _ROOT.$rowC['ruta_img_extra2'];
		$imgFooter = _ROOT.$rowC['ruta_img_footer'];
		$descripcionExtra = $rowC['descripcion_extra'];
	}else{
		?>
		<script type="text/javascript">
			window.location = '<?=_ROOT?>404';
		</script>
		<?php
	}

	$rowBlog = $link->query("SELECT * FROM blog WHERE link = '$url_noticia' AND status = 1 ORDER BY id DESC LIMIT 1 ")->fetch_array();
	$idBlog = $rowBlog['id'];
	$tituloBlog = $rowBlog['titulo'];
	$descripcionBlog = $rowBlog['descripcion'];
	$linkBlog = $rowBlog['link'];
	$imgBlog = _ROOT.$rowBlog['ruta_img'];

	$rowCat = $link->query("SELECT * FROM blog_categoria WHERE id = '".$rowBlog['id_categoria']."' LIMIT 1")->fetch_array();
	$tituloCategoria = $rowCat['titulo'];
	$linkCategoria = $rowCat['link'];

	$link -> query("
	    UPDATE  blog
	    SET     vista=vista+1
	    WHERE   id = '".$rowBlog['id']."'
	")or die(mysqli_error($link));

	$dia = date('d', strtotime($rowBlog['fecha_alta']));
	$mes = date('F', strtotime($rowBlog['fecha_alta']));
	$anio = date('Y', strtotime($rowBlog['fecha_alta']));

	if($mes == 'January') { $mes = 'Ene'; }
	if($mes == 'February') { $mes = 'Feb'; }
	if($mes == 'March') { $mes = 'Mar'; }
	if($mes == 'April') { $mes = 'Abr'; }
	if($mes == 'May') { $mes = 'May'; }
	if($mes == 'June') { $mes = 'Jun'; }
	if($mes == 'July') { $mes = 'Jul'; }
	if($mes == 'August') { $mes = 'Agosto'; }
	if($mes == 'September') { $mes = 'Sep'; }
	if($mes == 'October') { $mes = 'Oct'; }
	if($mes == 'November') { $mes = 'Nov'; }
	if($mes == 'December') { $mes = 'Dic'; }

	$fechaPost = $dia.' / '.$mes.' / '.$anio;
}else{
	?>
	<script type="text/javascript">
		window.location = '<?=_ROOT?>404';
	</script>
	<?php
}
?>
<style>
	body { background: #FFF; }
	header { background-color: rgba(0,0,0,.6); }
</style>
<div class="blog">
	<section class="seccion1">
		<div class="contenedor">
			<div class="conteLeft">
				<div class="imgPost">
					<img src="<?=$imgBlog?>" alt="<?=$tituloBlog?>">
					<ul class="col2 infoPost">
						<li>
							<a href="<?=_ROOT?>blog-categoria/<?=$linkCategoria?>/<?=$url_perfil?>"><i class="fas fa-folder"></i><?=$tituloCategoria?></a>
						</li>
						<li>
							<i class="fas fa-calendar-alt"></i><?=$fechaPost?>
						</li>
						<div class="clear"></div>
					</ul>
				</div>
				<h1 class="tituloPost"><?=$tituloBlog?></h1>
				<div class="descPost"><?=$descripcionBlog?></div>
				<div class="clear20"></div>
				<div class="conteCompartir">
					<h3>compartir</h3>
					<ul class="col4">
						<li>
							<a href="mailto:?body=¡Hola!%0A%0Ahe encontrado este nuevo artículo y pensé que podría interesarte.%0A%0A¡Saludos!%0A%0A<?= _ROOT.'post/'.$linkBlog.'/'.$url_perfil?>&subject=han compartido un artículo contigo" class="conte">
								<i class="fa fa-envelope" aria-hidden="true"></i> <span>EMAIL</span>
							</a>
						</li>
						<li>
							<a href="javascript:void(0)" class="conte" onclick="window.open('http://www.facebook.com/sharer/sharer.php?u=<?=_ROOT.'post/'.$linkBlog.'/'.$url_perfil?>','share','width=500,height=500')">
								<i class="fab fa-facebook-f" aria-hidden="true"></i>
							</a>
						</li>
						<li>
							<a href="javascript:void(0)" class="conte" onclick="window.open('https://twitter.com/intent/tweet?text=<?='Leyendo: '._ROOT.'post/'.$linkBlog.'/'.$url_perfil?>', 'share', 'width=500,height=300')">
								<i class="fab fa-twitter" aria-hidden="true"></i>
							</a>
						</li>
						<li>
							<a href="https://api.whatsapp.com/send?text=<?=_ROOT.'post/'.$linkBlog.'/'.$url_perfil?>" data-action="share/whatsapp/share" target="_blank" class="conte"><i class="fab fa-whatsapp" aria-hidden="true"></i></a>
						</li>
						<div class="clear"></div>
					</ul>
				</div>
			</div>
			<div class="conteRight"><?php include 'blog-derecho.php'; ?></div>
			<div class="clear"></div>
		</div>
	</section>

	<?php
	if(mysqli_num_rows($link->query("SELECT * FROM blog_relacionado WHERE id_blog = '$idBlog' ORDER BY id_relacion DESC"))>0){
		?>
		<section class="seccion2">
			<div class="contenedor">
				<div class="conteRelacionados">
					<h3>Artículos <br /> Relacionados</h3>
					<ul class="col2">
						<?php
						$query = $link->query("SELECT * FROM blog_relacionado WHERE id_blog = '$idBlog' ORDER BY id_relacion DESC");
						while($row = $query->fetch_array()) {
							$rowRel = $link->query("SELECT * FROM blog WHERE id = '".$row['id_relacion']."' LIMIT 1")->fetch_array();

							$dia = date('d', strtotime($rowRel['fecha_alta']));
							$mes = date('F', strtotime($rowRel['fecha_alta']));
							$anio = date('Y', strtotime($rowRel['fecha_alta']));
							if($mes == 'January') { $mes = 'Ene'; }
							if($mes == 'February') { $mes = 'Feb'; }
							if($mes == 'March') { $mes = 'Mar'; }
							if($mes == 'April') { $mes = 'Abr'; }
							if($mes == 'May') { $mes = 'May'; }
							if($mes == 'June') { $mes = 'Jun'; }
							if($mes == 'July') { $mes = 'Jul'; }
							if($mes == 'August') { $mes = 'Agosto'; }
							if($mes == 'September') { $mes = 'Sep'; }
							if($mes == 'October') { $mes = 'Oct'; }
							if($mes == 'November') { $mes = 'Nov'; }
							if($mes == 'December') { $mes = 'Dic'; }

							$fechaPost = $dia.' / '.$mes.' / '.$anio;
							?>
							<li>
								<div class="bgPostR" style="background-image: url(<?=_ROOT.$rowRel['ruta_img']?>);"></div>
								<div class="infoR">
									<div class="tituloR"><?=$rowRel['titulo']?></div>
									<div class="fechaR"><?=$fechaPost?></div>
									<div class="descR"><?=cambiaCadena(substr(strip_tags($rowRel['descripcion']),0, 100))?>...</div>
									<a href="<?=_ROOT?>post/<?=$rowRel['link']?>/<?=$url_perfil?>">ver más</a>
								</div>
							</li>
							<?php
						}
						?>
						<div class="clear"></div>
					</ul>
				</div>
			</div>
		</section>
		<?php
	}
	?>	
</div>
<?php include 'footer.php'; ?>