<?php include 'header.php'; ?>
<style type="text/css">
	body { padding-top: 0; }
	html, body { height: 100%; }
	header, footer { display: none; }
</style>
<div class="error">
	<div class="contenedor">
		<span>404</span>
		<p>Página no encontrada</p>
		<a href="<?=_ROOT?>" class="boton">Regresar</a>
	</div>
</div>
<?php include 'footer.php'; ?>