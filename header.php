<?php
include('php/conf.php');
include('php/funcionesPHP.php');
$pagina = basename($_SERVER['PHP_SELF']);
extract($_GET);
?>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta name="author" content="Ing. Jose Arturo / Wom Group">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="theme-color" content="#<?=$colorTema?>" />

	<meta http-equiv="Expires" content="0">
	<meta http-equiv="Last-Modified" content="0">
	<meta http-equiv="Cache-Control" content="no-cache, mustrevalidate">
	<meta http-equiv="Pragma" content="no-cache">
	<meta name="keywords" content="<?= $keywords; ?>">
	<meta name="description" content="<?= $desc; ?>">

	<title><?= $tituloCliente; ?></title>

	<!-- RESPONSIVO -->
	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />

	<!-- FAVICON -->
	<link rel="shortcut icon" type="image/x-icon" href="<?=_ROOT.$favicon?>">

	<!-- CSS -->
	<link rel="stylesheet" type="text/css" href="<?=_ROOT?>css/styles.css?'<?=rand(10, 10000);?>">

	<!-- JQUERY -->
	<script type="text/javascript" src="<?=_ROOT?>js/jquery-2.1.4.min.js"></script>
	<script type="text/javascript" src="<?=_ROOT?>js/jquery.validate.js"></script>
	<script type="text/javascript" src="<?=_ROOT?>js/jquery.form.js"></script>

	<!-- FONT AWESOME -->
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">

	<!-- FANCYBOX -->
	<link rel="stylesheet" type="text/css" href="<?=_ROOT?>lib/fancybox/jquery.fancybox.css">
	<script type="text/javascript" src="<?=_ROOT?>lib/fancybox/jquery.fancybox.pack.js"></script>

    <!-- SWEETALERT -->
    <link rel="stylesheet" type="text/css" href="<?=_ROOT?>lib/sweetalert/sweetalert.css">
    <script type="text/javascript" src="<?=_ROOT?>lib/sweetalert/sweetalert.min.js"></script>

    <!-- OWL CAROUSEL -->
	<script type="text/javascript" src="<?=_ROOT?>lib/owl_carousel/owl.carousel.js"></script>
	<link rel="stylesheet" type="text/css" href="<?=_ROOT?>lib/owl_carousel/owl.carousel.min.css">

    <script type="text/javascript" src="<?=_ROOT?>js/funciones.js?'<?=rand(10, 10000);?>"></script>
</head>
<body>

	<?php
	$url = 'https://newsapi.org/v2/everything?q=tesla&from=2021-07-09&sortBy=publishedAt&apiKey=d9fcaf73b9074385bf8ac781b9d7ddaa';
	$response = file_get_contents($url);
	$newData = json_decode($response);

	?>

<header>
	<?php include 'menu.php'; ?>
</header>